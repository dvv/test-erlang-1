-module(test_serve_cached_value_app).
-behaviour(application).

-export([start/2]).
-export([stop/1]).

-export([get_cached_value/0]).
-export([get_cached_value/1]).
-export([check/2]).

start(_Type, _Args) ->
  test_serve_cached_value_sup:start_link().

stop(_State) ->
  ok.

% ------------------------------------------------------------------------------

get_cached_value() ->
  gen_server:call(cache_server, {get_cache_value}).

get_cached_value(Timeout) ->
  try
    gen_server:call(cache_server, {get_cache_value}, Timeout)
  catch
    exit:{timeout, _} -> no_cache
  end.

% ------------------------------------------------------------------------------

get_cached_value_proc(From, Timeout) ->
  From ! {self(), get_cached_value(Timeout)}.

check(Times, Timeout) ->
  Me = self(),
  Pids = [erlang:spawn(fun() ->
      timer:sleep(rand:uniform(150)),
      get_cached_value_proc(Me, Timeout)
    end) || _ <- lists:seq(1, Times)],
  Res = [receive {_Pid, R} -> R end || _ <- Pids],
  Res.
