PROJECT = test_serve_cached_value
PROJECT_DESCRIPTION = Test for a highload caching server
PROJECT_VERSION = 0.1.0

# Whitespace to be used when creating files from templates.
SP = 2

include erlang.mk
