# Test of timely cached value getter

## Usage

```
$ make
$ make shell
1> application:start(test_serve_cached_value).
2> test_serve_cached_value_app:check(100, 100).
3> test_serve_cached_value_app:check(100, 1000).
```

The word `EVAL` means real `get_value` is called.
